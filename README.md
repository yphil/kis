## Kis synth ##

### Kis synth - based on SO-404 v.1.2 by 50m30n3 2009-2011 ###

**Kis** is a simple synthesizer using 1 oscillator and 1 filter.
The oscillator is a simple saw wave oscillator and the filter is a simple resonant lowpass filter. You know, like that other very good and famous synth with a similar name. Just not as good and famous.

![screenshot](https://bitbucket.org/yassinphilip/kis/raw/master/screenshot.png)

### Parameter list:
- CC#7	-	Volume
- CC#65	-	Portamento time
- CC#71	-	Filter Resonance
- CC#72	-	Release time
- CC#74	-	Filter Cutoff
- CC#79	-	Filter Envelope

If two notes are played together the pitch will slide according to the portamento time. Filter cutoff is influenced by the MIDI note velocity. A note velocity above 100 generates an accented note where the amp EG is replaced by the filter EG.

## Configure, build & install

## Dependancies

To install this package, you need a [C99-compliant compiler](http://gcc.gnu.org/), and the [LV2 headers](http://packages.debian.org/wheezy/lv2-dev) installed on your system.
You also need the [NTK toolkit dev files](http://github.com/original-male/ntk) installed.

### Debian packages

```
sudo apt install libjpeg-dev libcairo2-dev libxft-dev libfontconfig1-dev libx11-dev pkg-config lv2core
```

Kis uses the [waf](https://code.google.com/p/waf/) build system. Use

```
./waf configure --help
```
to see compilation options.

Building and installing generally goes like:

```
./waf configure
./waf
sudo ./waf install
```
Or, on non-sudo systems

```
su -c './waf install'
```

## Operate

By default, Kis is installed in `/usr/local/lib/lv2` ; its URI is `https://bitbucket.org/xaccrocheur/kis` so to test it with [jalv](http://drobilla.net/software/jalv/), enter

```
jalv https://bitbucket.org/xaccrocheur/kis
```
To see all LV2 plugins o your system, install [lilv-utils](http://packages.debian.org/stable/sound/lilv-utils) and enter

```
lv2ls
```

Then just insert it on a MIDI track in any MIDI-capable DAW (Tested in [Qtractor](http://qtractor.sourceforge.net/qtractor-index.html)).

Icon by [Buuf](http://mattahan.deviantart.com/art/Buuf-37966044)